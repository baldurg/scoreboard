// scoreboard.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

const char brighttranslate[] = { 0, 0, 75, 50, 25, 1 };

int opencomport(HANDLE &hPort, const char *port, unsigned baud) {
	COMMTIMEOUTS timeout;
	DCB dcb;

	char *myport = (char*)malloc(strlen(port) + 10);
	sprintf(myport, "\\\\.\\%s", port);

	hPort = CreateFileA(
		myport,
		GENERIC_WRITE | GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		//FILE_FLAG_OVERLAPPED,
		NULL
	);
	free((void*)myport);

	if (!GetCommState(hPort, &dcb)) {
		printf("Failed to open port %s\n", port);
		return 0;
	}

	dcb.BaudRate = baud; //9600 Baud
	dcb.ByteSize = 8; //8 data bits
	dcb.Parity = NOPARITY; //no parity
	dcb.StopBits = ONESTOPBIT; //1 stop

	dcb.fAbortOnError = false;               // Don't abort
	dcb.fBinary = true;                // Enable binary mode
	dcb.fDsrSensitivity = false;               // Ignore DSR line
	dcb.fDtrControl = DTR_CONTROL_DISABLE; // Disable DTR line
	dcb.fErrorChar = false;               // Don't replace bad chars
	dcb.fInX = false;               // Disable XIN
	dcb.fNull = false;               // Don't drop NULL bytes
	dcb.fOutX = false;               // Disable XOFF
	dcb.fOutxCtsFlow = false;               // Don't monitor CTS line
	dcb.fOutxDsrFlow = false;               // Don't monitor DSR line
	dcb.fParity = false;               // Disabled
	dcb.fRtsControl = RTS_CONTROL_DISABLE;
	dcb.wReserved = false;               // As per msdn.  Beware, not a member of _DCB in 8.0 and later header files.


	if (!SetCommState(hPort, &dcb)) {
		printf("Failed to set baud on port %s\n", port);
		CloseHandle(hPort);
		return 0;
	}

	timeout.ReadIntervalTimeout = 0; // ms between chars on read.
	timeout.ReadTotalTimeoutMultiplier = 0;
	timeout.ReadTotalTimeoutConstant = 250;

	timeout.WriteTotalTimeoutMultiplier = 2;
	timeout.WriteTotalTimeoutConstant = 25;

	if (!SetCommTimeouts(hPort, &timeout)) {
		printf("Failed to set timeouts on port %s\n", port);
		CloseHandle(hPort);
		return 0;
	}
	return 1;
}

void optecmessage(HANDLE &dst, int address, int bright, const char *message) {
	// @{P     }{D01,d}{N 012345012345,5>}{S DM=01,01}{Z}
	DWORD bytes;
	char buf[256];
	sprintf(buf, "@{P     }{D%02d,d}{N % 6s% 6s,xx}{S DM=%02d,%02d}{Z}", address, message, message, bright, bright);
	unsigned cksum = 0;
	for (int i = 19; i < 31; i++) {
//		printf("Checking char '%c'\n", buf[i]);
		cksum += buf[i];
	}
//	printf("cksum is %d\n", cksum);
	buf[32] = 0x30 + ((cksum >> 4) & 0xF);
	buf[33] = 0x30 + (cksum & 0xF);
	printf("%s\n", buf);
	WriteFile(dst, buf, strlen(buf), &bytes, NULL);
}

int main(int argc, char **argv)
{
	HANDLE optecport, ptport;
	DWORD bytes;
	char rxbuf[256];
	char msgbuf[512];
	char msgstring[16];
	msgbuf[256] = '\0';
	unsigned char synccount = 0;
	unsigned char offset = 0;
	bool syncstate = false;

	printf("Scoreboard driver by Baldur Gislason, %d built %s %s\n", argc, __DATE__, __TIME__);

	if (argc < 3) {
		printf("Usage: %s <Optec COM port> <Port a tree COM port> [optional]\n"
			"Optional arguments:\n"
			"test: Light up all digits with the number 8 and exit\n"
			"OR Alternatively: %s <Optec COM port> transmit <address> <brightness> <message>\n", argv[0], argv[0]);
		return 1;
	}
	printf("Opening Optec port '%s', Portatree port '%s'\n", argv[1], argv[2]);
	if (!opencomport(optecport, argv[1], 9600)) return 1;
	if (!strcmp(argv[2], "transmit")) {
		optecmessage(optecport, atoi(argv[3]), atoi(argv[4]), argv[5]);
		return 0;
	}
	if (!opencomport(ptport, argv[2], 9600)) return 1;
	optecmessage(optecport, 1, 0, "88888");
	optecmessage(optecport, 2, 0, "88888");
	optecmessage(optecport, 3, 0, "88888");
	optecmessage(optecport, 4, 0, "88888");
	optecmessage(optecport, 5, 0, "88888");
	if (argc > 3 && !strcmp(argv[3], "test")) return 0;
	Sleep(2000);
	optecmessage(optecport, 1, 0, "11111");
	optecmessage(optecport, 2, 0, "22222");
	optecmessage(optecport, 3, 0, "33333");
	optecmessage(optecport, 4, 0, "44444");
	optecmessage(optecport, 5, 0, "55555");
	unsigned char address = 0;
	unsigned char lastaddr = 0;
	unsigned char brightness = 0;
	while (true) {
		ReadFile(ptport, rxbuf, 128, &bytes, 0);
		if (bytes) {
			rxbuf[bytes] = '\0';
			printf("Received '%s'\n", rxbuf);
			for (unsigned i = 0; i < bytes; i++) {
				if (rxbuf[i] == '@') synccount++;
				else synccount = 0;
				if (synccount >= 3) {
					offset = 0;
					syncstate = true;
				}
				else if(syncstate) {
					msgbuf[offset++ & 0xFF] = rxbuf[i];
					if (offset > 10 && msgbuf[(offset - 1) & 0xFF] == '}' && msgbuf[(offset - 2) & 0xFF] == 'E' && msgbuf[(offset - 3) & 0xFF] == '{') {
						msgbuf[offset] = '\0';
						if (char *origin = strstr(msgbuf, "{D")) { // Find our address
							address = origin[2] - 0x30;
							if (char *origin = strstr(msgbuf, "{S DM=")) {
								if (char *end = strchr(origin, '}')) {
									end[0] = '\0';
									brightness = atoi(origin + 6);
									end[0] = '}';
									if (char *origin = strstr(msgbuf, "{N ")) {
										if (char *end = strchr(origin, '}')) {
											end[0] = '\0';
											origin += 3;
											int d = 0;
											for(int o = 0; d < 10 && origin[o]; o++) {
												if (origin[o] >= 0x30 && origin[o] <= 0x39) { // Only accept numbers
													msgstring[d++] = origin[o];
												}
											}
											msgstring[d] = '\0';
											end[0] = '}';
											printf("\nHave message '%s'\nADDRESS %d BRIGHT %d MSG '%s'\n", msgbuf, address, brightness, msgstring);
											if (brightness <= 5) brightness = brighttranslate[brightness];
											//if (lastaddr == address) Sleep(200);
											optecmessage(optecport, address, brightness, msgstring);
											Sleep(200);
											lastaddr = address;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//else {
		//	printf("Received nuthing\n");
		//}
		Sleep(1);
	}
    return 0;
}

